<table width="30%" height="60" border="0" align="center">
 
  <center><h2 style="font-family:'Comic Sans MS', cursive">Input Karyawan</h2></center>

  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST" enctype="multipart/form-data">
<table width="40%" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <br/>  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" value="<?=set_value('nik');?>" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" value="<?=set_value('nama_lengkap');?>" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=set_value('tempat_lahir');?>" maxlength="50"></td>
    </td>
  </tr>
  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
       <option value="L">Laki laki</option>
       <option value="P">Perempuan</option>
      
    </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		?>
        <option value="<?=$tgl;?>"><?=$tgl;?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		?>
        <option value="<?=$bln+1;?>">
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
		?>
        <option value="<?=$thn;?>"><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=set_value('telp');?>"></td>
    </td>
  </tr>
 
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=set_value('alamat');?></textarea></td>
  </tr>
 <tr>
    <td height="35">Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
    <?php foreach($data_jabatan as $data) {?>
       <option value="<?= $data->kode_jabatan;?>">
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
   <tr>
    <td style="text-align:left;">Upload Foto</td>
    <td>:</td>
    <td style="text-align:left;">
    <input type="file" name="image" id="image" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" value="reset" style="background-color:#F00">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</table>
</form>
